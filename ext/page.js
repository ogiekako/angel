
const INTERVAL = 5000 // millis
const URL = 'http://localhost:8000'

function postBlob(blob) {
  const method = 'POST'
  //送信先を指定
  const headers = {
    'Content-Type': 'application/x-www-form-urlencoded'
  };
  // 送信データを変換
  // const obj = jsondata //json形式
  // const body = Object.keys(obj).map((key)=>key+"="+encodeURIComponent(obj[key])).join("&");
  const body = blob

  const options = {
    method, 
    headers,
    mode: 'cors',
    body
  }
  fetch(URL, options)
    .then(res => {
      //成功時の処理
      console.log(res)
    })
  .catch(err => {
    //エラー時の処理
    console.error
  })
}

function postScreenshot() {
     var player = document.getElementsByClassName("video-stream")[0];
     var canvas = document.createElement("canvas");
     canvas.width = player.videoWidth;
     canvas.height = player.videoHeight;
     canvas.getContext('2d')
         .drawImage(player, 0, 0, canvas.width, canvas.height);
     canvas.toBlob(function(blob) {
       postBlob(blob)
     }, 'image/png');
}

setInterval(postScreenshot, INTERVAL)
