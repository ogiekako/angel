#!/usr/bin/env python
import cognitive_face as CF
import requests
import urllib
import httplib
import json
import sys

CHILD = 9


def has_child(img):
    KEY = '4f1f8c0dd1014782923377e91b269a6b'  # Replace with a valid subscription key (keeping the quotes in place).
    BASE_URL = 'https://westcentralus.api.cognitive.microsoft.com/face/v1.0'  # Replace with your regional Base URL

    headers = {
        'Content-Type': 'application/octet-stream',
        'Ocp-Apim-Subscription-Key': KEY,
    }
    url = 'https://api.projectoxford.ai/face/v1.0/detect'
    params = urllib.urlencode({
        'returnFaceAttributes': 'gender,age',
    })

    conn = httplib.HTTPSConnection('westcentralus.api.cognitive.microsoft.com')
    conn.request("POST", "/face/v1.0/detect?%s" % params, open(img, 'rb'),
                 headers)
    response = conn.getresponse()
    data = response.read()
    result = json.loads(data)

    print 'there are %d people' % len(result)
    for a in result:
        age = a['faceAttributes']['age']
        print age
        if age <= CHILD:
            return True
    return False


if __name__ == '__main__':
    print 'has child' if has_child(sys.argv[1]) else 'no child'
