# -*- coding: utf-8 -*-

import check
import datetime
import requests
import urllib
import json

import upload
import warn

URL = 'https://dev50588.service-now.com/api/now/table/x_165734_childinth_child_discovery'
LINE = 'https://maker.ifttt.com/trigger/child_detected/with/key/cyOvPsWd4-a8AKymIa3trC'

# test
# LINE = 'https://maker.ifttt.com/trigger/child_detected/with/key/dxhkn6kF7tXN2XJkDY8pQ9'

STORE = 'STR0001001'

PUBLIC_DIR = 'https://storage.googleapis.com/child-saver'

testimg = 'https://storage.googleapis.com/child-saver/blogs.mighty.safety.jpg'


def line(tlabel, store, img):
    # Value1: img
    # Value2: message
    # Value3: time label
    h = {
        'value1': img,
        'value2': 'Child detected',
        'value3': tlabel,
    }
    headers = {'Content-Type': "application/json"}
    resp = requests.post(LINE, json=h, headers=headers)
    print resp.status_code


def post(tlabel, store):
    p = {"discovered": tlabel, "store": store}
    d = json.dumps(p)
    resp = requests.post(URL, data=d, auth=('admin', 'Hack1234'))

    print resp.json()


def doit(imgblob):
    t = datetime.datetime.now()
    tlabel = t.strftime('%Y-%m-%d %H:%M:%S')
    path = (tlabel + '_face.png').replace(' ', '_')
    local = '/tmp/' + path
    remote = 'gs://child-saver/' + path
    with open(local, 'w') as f:
        f.write(imgblob)
    has_child = check.has_child(local)
    if not has_child:
        return

    post(tlabel, STORE)  # post to database
    upload.upload(local, remote)
    public_url = PUBLIC_DIR + '/' + path
    line(tlabel, STORE, public_url)
    warn.warn()


if __name__ == '__main__':
    line('2007-12-31 00:00:00', 'STR0001001', testimg)
