from subprocess import call

def upload(local, remote):
    call(["gsutil","cp",local,remote])
    call(["gsutil","acl","ch","-u","AllUsers:R", remote])

if __name__ == '__main__':
    upload('/tmp/test.png', 'gs://child-saver/test.png')
